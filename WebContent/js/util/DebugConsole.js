/**
 * @constructor
 * 
 * @param{String} divId the id of the div where console output will be displayed
 */
function DebugConsole(divId) {
	
	this.div = $('#'+divId);
	this.linesCount = 0;
	
	/**
	 * @public
	 */
	this.toConsole = function(message) {
		if(this.linesCount>10) {
			this.clearConsole();
		}
		this.div.append(message+"</br>");
		this.linesCount++;
	};
	
	/**
	 * @public
	 */
	this.clearConsole = function() {
		this.div.empty();	
		this.linesCount = 0;
	};
	
}