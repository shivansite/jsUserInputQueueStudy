function Animation(img, animationFramesCrops, fps, wrapAround) {
	this.img = img;
	this.animationFramesCrops = animationFramesCrops;
	this.fps = fps;
	this.frameIdxIncrem = Math.floor(FIXED_UPDATES_IN_A_SECOND/this.fps);
	this.wrapAround = wrapAround;
	
	this.gameStateUpdatesCount = -1;
	
	this.start = function() {
		this.gameStateUpdatesCount = 0;
	};
	
	this.isOver = function() {
		return this.gameStateUpdatesCount > this.animationFramesCrops.length * this.frameIdxIncrem;
	};
	
	this.onGameStateUpdate = function() {
		if(this.gameStateUpdatesCount>=0) {
			this.gameStateUpdatesCount++;
		}
	};
	
	this.onGraphicsUpdate = function(context, x, y) {
		if(this.gameStateUpdatesCount>=0) {
			var frameCrop = this.animationFramesCrops[this.getFrameIdx()];
			context.drawImage(this.img,
					frameCrop.x, frameCrop.y, frameCrop.width, frameCrop.height, 
					x+frameCrop.dx, y+frameCrop.dy, frameCrop.width, frameCrop.height);
		}
	};
	
	this.getFrameIdx = function() {
		if(this.gameStateUpdatesCount>=0) {
			var idx = Math.floor(this.gameStateUpdatesCount / this.frameIdxIncrem);
			if(this.wrapAround) {
				return idx % this.animationFramesCrops.length;
			} else {
				return idx < this.animationFramesCrops.length ? idx : this.animationFramesCrops.length-1; 
			}
		};
	};
	
};

function ImgCrop(x, y, dx, dy, width, height) {
	this.x = x;
	this.y = y;
	this.dx = dx;
	this.dy = dy;
	this.width = width;
	this.height = height;
}

function AnimationManager() {

	this.createStandAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_01;		
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_02;		
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_03;		
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_02;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 6, true);
		return animation;	
	};
	
	this.createWalkAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_01;		
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_02;		
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_03;		
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_02;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 12, true);
		return animation;	
	};
	
	this.createCrouchAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_37;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 1, true);
		return animation;	
	};
	
	this.createPunchAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_10;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 10, false);
		return animation;
	};
	
	this.createSimpleKickAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_04;		
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_05;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_04;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 10, false);
		return animation;
	};
	
	this.createRoundhouseKickAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_06;		
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_07;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_08;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 10, false);
		return animation;
	};
	
	this.createStaffKataAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_25;
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_26;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_26;
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_25;	
		animationFramesCrops[4] = SPRITE_SHEET_CROPPED_FRAME_19;		
		animationFramesCrops[5] = SPRITE_SHEET_CROPPED_FRAME_20;
		animationFramesCrops[6] = SPRITE_SHEET_CROPPED_FRAME_21;
		animationFramesCrops[7] = SPRITE_SHEET_CROPPED_FRAME_22;		
		animationFramesCrops[8] = SPRITE_SHEET_CROPPED_FRAME_23;
		animationFramesCrops[9] = SPRITE_SHEET_CROPPED_FRAME_24;
		animationFramesCrops[10] = SPRITE_SHEET_CROPPED_FRAME_25;	
		animationFramesCrops[11] = SPRITE_SHEET_CROPPED_FRAME_19;		
		animationFramesCrops[12] = SPRITE_SHEET_CROPPED_FRAME_20;
		animationFramesCrops[13] = SPRITE_SHEET_CROPPED_FRAME_21;
		animationFramesCrops[14] = SPRITE_SHEET_CROPPED_FRAME_22;		
		animationFramesCrops[15] = SPRITE_SHEET_CROPPED_FRAME_23;
		animationFramesCrops[16] = SPRITE_SHEET_CROPPED_FRAME_24;
		animationFramesCrops[17] = SPRITE_SHEET_CROPPED_FRAME_25;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 14, false);
		return animation;
	};
	
	this.createHeliKickAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_37;
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_27;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_28;
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[4] = SPRITE_SHEET_CROPPED_FRAME_30;		
		animationFramesCrops[5] = SPRITE_SHEET_CROPPED_FRAME_31;
		animationFramesCrops[6] = SPRITE_SHEET_CROPPED_FRAME_32;
		animationFramesCrops[7] = SPRITE_SHEET_CROPPED_FRAME_33;		
		animationFramesCrops[8] = SPRITE_SHEET_CROPPED_FRAME_34;
		animationFramesCrops[9] = SPRITE_SHEET_CROPPED_FRAME_35;
		animationFramesCrops[10] = SPRITE_SHEET_CROPPED_FRAME_36;
		animationFramesCrops[11] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[12] = SPRITE_SHEET_CROPPED_FRAME_30;		
		animationFramesCrops[13] = SPRITE_SHEET_CROPPED_FRAME_31;
		animationFramesCrops[14] = SPRITE_SHEET_CROPPED_FRAME_32;
		animationFramesCrops[15] = SPRITE_SHEET_CROPPED_FRAME_33;		
		animationFramesCrops[16] = SPRITE_SHEET_CROPPED_FRAME_34;
		animationFramesCrops[17] = SPRITE_SHEET_CROPPED_FRAME_35;
		animationFramesCrops[18] = SPRITE_SHEET_CROPPED_FRAME_36;
		animationFramesCrops[19] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[20] = SPRITE_SHEET_CROPPED_FRAME_28;
		animationFramesCrops[21] = SPRITE_SHEET_CROPPED_FRAME_27;
		animationFramesCrops[22] = SPRITE_SHEET_CROPPED_FRAME_37;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 16, false);
		return animation;
	};
	
	this.createHeliKickAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_37;
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_27;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_28;
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[4] = SPRITE_SHEET_CROPPED_FRAME_30;		
		animationFramesCrops[5] = SPRITE_SHEET_CROPPED_FRAME_31;
		animationFramesCrops[6] = SPRITE_SHEET_CROPPED_FRAME_32;
		animationFramesCrops[7] = SPRITE_SHEET_CROPPED_FRAME_33;		
		animationFramesCrops[8] = SPRITE_SHEET_CROPPED_FRAME_34;
		animationFramesCrops[9] = SPRITE_SHEET_CROPPED_FRAME_35;
		animationFramesCrops[10] = SPRITE_SHEET_CROPPED_FRAME_36;
		animationFramesCrops[11] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[12] = SPRITE_SHEET_CROPPED_FRAME_30;		
		animationFramesCrops[13] = SPRITE_SHEET_CROPPED_FRAME_31;
		animationFramesCrops[14] = SPRITE_SHEET_CROPPED_FRAME_32;
		animationFramesCrops[15] = SPRITE_SHEET_CROPPED_FRAME_33;		
		animationFramesCrops[16] = SPRITE_SHEET_CROPPED_FRAME_34;
		animationFramesCrops[17] = SPRITE_SHEET_CROPPED_FRAME_35;
		animationFramesCrops[18] = SPRITE_SHEET_CROPPED_FRAME_36;
		animationFramesCrops[19] = SPRITE_SHEET_CROPPED_FRAME_29;	
		animationFramesCrops[20] = SPRITE_SHEET_CROPPED_FRAME_28;
		animationFramesCrops[21] = SPRITE_SHEET_CROPPED_FRAME_27;
		animationFramesCrops[22] = SPRITE_SHEET_CROPPED_FRAME_37;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 16, false);
		return animation;
	};
	
	this.createStaffSwirlAnimation = function() {
		var animationFramesCrops = new Array();
		
		animationFramesCrops[0] = SPRITE_SHEET_CROPPED_FRAME_11;
		animationFramesCrops[1] = SPRITE_SHEET_CROPPED_FRAME_12;
		animationFramesCrops[2] = SPRITE_SHEET_CROPPED_FRAME_13;
		animationFramesCrops[3] = SPRITE_SHEET_CROPPED_FRAME_14;	
		animationFramesCrops[4] = SPRITE_SHEET_CROPPED_FRAME_15;		
		animationFramesCrops[5] = SPRITE_SHEET_CROPPED_FRAME_16;
		animationFramesCrops[6] = SPRITE_SHEET_CROPPED_FRAME_17;
		animationFramesCrops[7] = SPRITE_SHEET_CROPPED_FRAME_18;		
		animationFramesCrops[8] = SPRITE_SHEET_CROPPED_FRAME_17;
		animationFramesCrops[9] = SPRITE_SHEET_CROPPED_FRAME_16;
		animationFramesCrops[10] = SPRITE_SHEET_CROPPED_FRAME_15;
		animationFramesCrops[11] = SPRITE_SHEET_CROPPED_FRAME_14;	
		animationFramesCrops[12] = SPRITE_SHEET_CROPPED_FRAME_13;		
		animationFramesCrops[13] = SPRITE_SHEET_CROPPED_FRAME_12;
		animationFramesCrops[14] = SPRITE_SHEET_CROPPED_FRAME_11;
		animationFramesCrops[15] = SPRITE_SHEET_CROPPED_FRAME_11;
		animationFramesCrops[16] = SPRITE_SHEET_CROPPED_FRAME_11;
		
		var animation = new Animation(SPRITE_CATALOG_DONNIE, animationFramesCrops, 16, false);
		return animation;
	};
	
}