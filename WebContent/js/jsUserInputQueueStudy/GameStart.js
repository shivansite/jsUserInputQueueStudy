function main() {
	$(document).focus();
	createInputQueueGame();
	createBestEffortInputGame();
}

function createInputQueueGame() {
	var gameEntity = new GameEntity(70, 110, 100, 100);
	
	var gameEntities = new Array();
	gameEntities[0] = gameEntity;
	
	var inputQueue = new Queue(USER_INPUT_BUFFER_CAPACITY);
	var gameLoop = new InputQueueGameLoop(gameEntities, inputQueue, "canvas1");
	var keyboardListener = new QueuedKeyboardListener(inputQueue, "canvas1");
	
	gameLoop.start();
}
	

function createBestEffortInputGame() {
	var gameEntity = new GameEntity(70, 110, 100, 100);
	
	var gameEntities = new Array();
	gameEntities[0] = gameEntity;
	
	var gameLoop = new BestEffortInputGameLoop(gameEntities, "canvas2");
	var keyboardListener = new BestEffortKeyboardListener(gameLoop, "canvas2");
	
	gameLoop.start();
}